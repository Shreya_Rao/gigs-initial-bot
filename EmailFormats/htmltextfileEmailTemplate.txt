﻿<html>
<body>
<p>Hello {0},<br/>
<br/>
Thank you for submitting your information and documents to <b>Sterling</b> who are currently in the process of completing your background check as instructed by <b>{1}</b>.
 <br/>
<br/> If you wish to move forward with your application, we request you to provide the below additional information:<br/>
<br/>
<b>Missing Information :<br/><br/>  {5}</b>
 
<br/><br/>Please return the required documentation/information via email by <b><u>replying to this message</u></b> or writing to us at Eats_MI@sterlingts.com. <br/>
<br/>We may follow-up with you later this week if no response is received, so as not to delay your overall <b>{1}</b> Courier application.<br/>

<br/>Sterling Internal Reference: Req. id # {2} | Country: {3}| Prod Desc: {4}<br/>

<br/>Regards,
<br/>Missing Info Team<br/>
<b>Sterling</b><br/>
www.sterlingcheck.com<br/>
<i></i>
</p>
</body>
</html>
